fig, ax = plt.subplots()
ax.plot(x, y)
ax.plot(x, np.poly1d(np.polyfit(x, y, 1))(x))
ax.spines['left'].set_position('zero')
ax.spines['bottom'].set_position('zero')
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
ax.set_xlabel('Label for x-axis')
ax.set_ylabel('Label for y-axis')
plt.title('Title')
plt.tight_layout()
plt.tick_params(
    axis='x',
    which='both',
    bottom='on',
    top='off')
plt.tick_params(
    axis='y',
    which='both',
    left='on',
    right='off')
# ax.set_xlim([a,b])
# ax.set_ylim([c,d])
# plt.show()
fig.savefig(fig)
