ffibuilder = FFI()
# ulimit -c unlimited #don't limit size of core files
# add -g to gcc for debugging info
# ls -lat /cores #find core number
# gdb a.out core.77125
# might need to prefix /cores/
# get error information from core number 77125

ffibuilder.cdef("int function_name(int);")

ffibuilder.set_source("file_name_compiled",
                      """
                          #include <gmp.h>
                          #include <float.h>
                          #include <stdio.h>
                          #include <math.h>
                          #include <stdlib.h>
                          static int function_name(int)
                          {
                              return 0;
                          }
                       """,
                      extra_link_args=['-lgmp', '-lm'])

ffibuilder.compile()
